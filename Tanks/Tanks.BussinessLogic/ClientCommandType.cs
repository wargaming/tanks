﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks.BussinessLogic
{
    public enum ClientCommandType
    {
        None,
        Go,
        Stop,
        TurnRight,
        TurnLeft,
        TurnUp,
        TurnDown,
        Fire,
        Die
    }
}
