﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks.BussinessLogic
{
    public class Tank : BaseDynamicObject
    {
        public string Tag { get; set; }
        public int MaxHP { get; set; }
        public int CurrentHP { get; set; }
    
    }
}
