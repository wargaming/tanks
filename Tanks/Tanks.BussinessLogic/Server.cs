﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Tanks.BussinessLogic
{
    public class DynamicObjects : List<BaseDynamicObject>
    {

    }

    public delegate ClientCommandType ClientDelegate(MapInfo map, DynamicObjects dynamicObjects);

    public class MapInfo : List<List<CallType>>
    {
        public int MapWidth { get; set; }
        public int MapLength { get; set; }
        
        public CallType this[int x, int y]
        {
            get
            {
                if (x < 0 
                    || x > MapLength 
                    || y < 0 
                    || y > MapLength)
                {
                    throw new IndexOutOfRangeException("out");
                }

                return this[y, x];
            }
        }
    }

    public class Server
    {
        public MapInfo Map { get; set; }
        public DynamicObjects DynamicObjects { get; set; }
        public List<ClientDelegate> Clients;
        private Random _rnd = new Random();

        public Tank addClient(ClientDelegate client)
        {
            if (client == null)
            {
                throw new ArgumentNullException("client");
            }

            Clients.Add(client);
            var result = new Tank();
            result.Id = Guid.NewGuid();

            while (true)
            {

                var x = _rnd.Next(Map.MapWidth);
                var y = _rnd.Next(Map.MapLength);
                
                if (!CanPlaceTank(x, y))
                {
                    continue;
                }
                break;
            }

            return result;
        }

        public void MapInit(int widht, int length)
        {
            Map = new MapInfo();
            var lines = File.ReadAllLines("file.txt");

            foreach (var line in lines)
            {
                var mapLine = new List<CallType>();
                Map.Add(mapLine);

                foreach (var c in line)
                {
                    switch (c)
                    {
                        case ' ':
                            mapLine.Add(CallType.Empty);
                            break;
                        case 'т':
                            mapLine.Add(CallType.Grass);
                            break;
                        case 'в':
                            mapLine.Add(CallType.Water);
                            break;
                        case 'с':
                            mapLine.Add(CallType.Wall);
                            break;
                        default:
                            throw new Exception("не подходящий тип ячейки");
                    }
                }

            };

            Map.MapWidth = Map.First().Count;
            Map.MapLength = Map.Count;

        }

        private bool CanPlaceTank(int x, int y)
        {
            var canPlace = Map[x, y] == CallType.Empty || Map[x, y] == CallType.Grass;

            foreach (var obj in DynamicObjects)
            {
                if (obj.X == x && obj.Y == y)
                {
                    canPlace = false;
                }
            }
        
            return canPlace;
        }
    }
}