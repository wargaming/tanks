﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tanks.BussinessLogic
{
    public abstract class BaseDynamicObject
    {
        public Guid Id { get; set; }
        public int X { get; set; }
        public int Y { get; set; }

    }
}
